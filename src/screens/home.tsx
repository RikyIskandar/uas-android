import React from 'react'
import { View, Text, Image, TouchableOpacity,ScrollView } from 'react-native'

interface HomeProps {
    navigation: any;
  }
const Home = (props:HomeProps) => {
    return (
        <ScrollView>
        <View>
                <View style={{backgroundColor:'#d7ccc8',height:510,justifyContent:'center',alignItems:'center' }}>
                    <View style={{
                        justifyContent:'center',
                        alignItems:'center', 
                        height:60,
                        width:350,
                        // borderWidth:2,
                        borderColor:'#FFFFFF',
                        borderRadius:10,
                        marginTop:1}}>

                        <Text style={{fontSize:30,fontWeight:'bold' }}>
                        MY QUR'AN</Text>
                    </View>
                    <View style={{justifyContent:'center',
                        alignItems:'center', 
                        height:270,
                        width:270, 
                        borderRadius:150,
                        marginTop:60,
                        paddingTop:60,
                        marginBottom:50}}>
                        <Image 
                        source={require('../gambar/2.png')}
                        style={{width:300,height:400}}
                        />
                    </View>            
                </View>
                <View style={{flexDirection:'row',backgroundColor:'#d7ccc8',height:210,flexWrap:'wrap',justifyContent:'center',alignItems:'center',}}>
                <View >
                        <TouchableOpacity
                                  onPress = {() => 
                                    props.navigation.navigate('Home')
                                }>

                    <View style={{
                            justifyContent:'center',
                            alignItems:'center', 
                            height:100, 
                            width:110,
                            marginTop:20,                           
                            }}>

                        <Image 
                        source={require('../gambar/3.png')}
                        style={{width:60,height:60}}
                        />
                        <Text style={{fontSize:15,fontWeight:'bold' }}>DAFTAR SURAH</Text>
                    </View>
                        </TouchableOpacity>
                    </View>

                    <View>
                        <TouchableOpacity
                        onPress = {() => 
                            props.navigation.navigate('Tahlil')
                        }>

                    <View style={{
                            justifyContent:'center',
                            alignItems:'center', 
                            height:100, 
                            width:110,
                            marginTop:20,
                            marginLeft:10}}>

                    <Image 
                        source={require('../gambar/dzikir2.png')}
                        style={{width:60,height:60}}
                        />
                        <Text style={{fontSize:15,fontWeight:'bold' }}>DZIKIR</Text>
                    </View>
                        </TouchableOpacity>
                    </View>

                    
                    
                    </View>

                    
                   
        </View>
        </ScrollView>
    )
}

export default Home




