import { StackActions } from '@react-navigation/native';
import React, {useEffect} from 'react';
import { View, Text,Image} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';



interface SplashProps {
    navigation: any;
  }

const Splash = (props:SplashProps) => {
    useEffect(()=> {
        setTimeout(() => {
             props.navigation.dispatch(StackActions.replace('Awal'))
        }, 3000)
    })
    return (
        <ScrollView>
        <View style={{justifyContent:'center',alignItems:'center',height:710,backgroundColor:'#d7ccc8'}}>
            <View style={{justifyContent:'center',alignItems:'center'}}>           
            <Image source={require('../gambar/as.png')}
            style={{
                width:350,
                height:350,
                }}></Image>
            </View>
        </View>
        </ScrollView>
    );
};

export default Splash;