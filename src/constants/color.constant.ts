const Colors = {
  white: '#FFF',
  purple1: '#8A46E9',
  purple2: '#6632BB',
  purple3: '#893FD5',
  grey1: '#F0F4F6',
  grey2: '#868AA3',
  grey3: '#F9F5FB',
  line: '#F6F8FA',
  cream1: '#d7ccc8',
  cream2: '#fff3e0',
  cream3: '#ccc5af',
  black: '#212121'
};

export default Colors;
